# vfs.libtorrent

This is a [Kodi](https://kodi.tv/) VFS (Virtual File System) add-on to support
the BitTorrent protocol thanks to [libtorrent](https://libtorrent.org).

[![License:
GPL-3.0](https://img.shields.io/badge/License-GPL%20v3-blue.svg)](LICENSE.md)

**This add-on is in development and no official version was released yet.**  
**The current status is available
[here](https://framagit.org/thombet/vfs.libtorrent/-/issues/1).**

## Features

* Download torrent files using HTTPs URL pointing to a .torrent file
* Download the whole torrent or only a part of it in background

## How to install this add-on

This add-on is not available in any repository yet so the only solution to
install it is first to compile it for your system.

Please read [building.md](./doc/building.md) for more information.
 
## Usage

The only way to use this add-on is via the
[xbmcvfs](https://alwinesch.github.io/group__python__xbmcvfs.html) python
library.

Here is an example

```python
# Create an URL-encoded path of the torrent to download with the protocol as prefix
vfs_url = "torrent://{}".format(quote_plus(torrent_url))

# Call the add-on via xbmcvfs library
torrent = xbmcvfs.File(vfs_url)

# Start downloading the file
if torrent.seek(1, 0):

  # Get information about the torrent
  torrent_info = json.loads(torrent.read())

  # [...]
  # Do something with the downloaded file
```

For more information, please read the [usage doc](./doc/usage.md).