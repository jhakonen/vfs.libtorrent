# How to use this add-on

[[_TOC_]]

## Overview

The main goal of this add-on is to support the BitTorrent protocol by providing
the Kodi python add-ons with an interface to
[libtorrent](https://www.libtorrent.org/) in a maintainable way (i.e. compatible
with all the platforms/systems quite easily).

Initially this add-on was developed for the
[PeerTube adon](https://framagit.org/StCyr/plugin.video.peertube) but it could
be used for any other usage.  
Even if PeerTube is a video streaming service, note that this add-on supports
only download, not streaming.

This add-on extends the
[xbmcvfs](https://alwinesch.github.io/group__python__xbmcvfs.html) python
library by making some of the existing methods of this module compatible with
torrent files.  
To use it you only have to call one of the supported xbmcvfs method with a
path/URL prefixed with `torrent://`.

## Supported APIs

A VFS add-on normally extends the Kodi file system to support additional
extensions or protocols.  
This add-on downloads files using the BitTorrent protocol that is to say a first
file containing metadata (.torrent file or .magnet URL) will be used to download
other files.  
This usage does not fit exactly to the classic actions of a file system (open,
read, write, etc.) so this add-on does not exactly use the xbmcvfs APIs for
their initial purpose.

### xbmcvfs.File()


```python
torrent = xbmcvfs.File(torrent_url)
```

The python constructor `xbmcvfs.File()` calls the C++ `Open()` API.

It will add to the libttorrent session the torrent that is passed as parameter.  
Only HTTPS URL pointing to .torrent files are currently supported (for instance
`https://webtorrent.io/torrents/wired-cd.torrent`): magnet links and local
.torrent files are not supported.

The metadata of the torrent will be downloaded but no other files will be
downloaded.  
If the metadata takes more than 30 seconds to be downloaded, the function will
time out and exit with an error BUT a valid `xbmcvfs.File object` will be
returned anyway... Currently there is no way to detect this error on the python
side. As a workaround, in case of time-out the torrent is removed from the
libtorrent session before exiting the `Open()` API so calling any other API
should fail. It should force the user to try again to call the constructor which
will hopefully download the metadata successfully.

If the torrent pointed by the URL already exists in the libtorrent session,
the function will be exited quite fast and without errors.

Note: the optional parameter `mode` is not used. Using the read or the write
mode will not change anything.

### xbmcvfs.File().read()

```python
import json

torrent = xbmcvfs.File(torrent_url)
info = json.loads(torrent.read())
```

This function will return a JSON string which contains information about the
torrent. It may be easily converted to a dictionnary using the python JSON
module.

The schema of the JSON string is documented in the function `generate_info_json`
which is defined in the file [TorrentFile.cpp](../src/TorrentFile.cpp).

### xbmcvfs.File().seek()


```python
torrent = xbmcvfs.File(torrent_url)
if torrent.seek(1*100, 0):
  # Actions when 1% of the torrent was downloaded successfully
else:
  # Actions when the download of the torrent could not be started/resumed
```

This API can be used to control the torrent (i.e. pause/resume/start) that was
initialized by `File()`.

This function takes 2 arguments but the second parameter may be omitted or `0`
may be used. Note that using `2` will also call the C++ `GetLength()` API which
will slightly decrease the performance (with `0` only the C++ APIs `GetPosition
()` and `Seek()` will be called.)

The first argument accepts the following values:
* `-1` &rarr; the download of the torrent will be paused
* from `0` to `10000` &rarr; start/resume the download of the torrent and wait
  for X% of the torrent to be downloaded

When starting/resuming the torrent, only a chunk of the torrent will be
downloaded in foreground: the size of this chunk is passed as argument.  The
`seek()` function will not return until this chunk is downloaded which allows
the call to control the behavior accurately.

The size of the first chunk is defined in proportion of the torrent: the agument
divided by 100 is a percentage. For instance:
* using `10000` will download the full torrent in foreground (100%)
* using `500` will return when 5% of the torrent is downloaded
* using `0` will return immediately

Note that if you want to play the media that is being downloaded, `0%` will
probably never work because the `seek()` function will return so early that a
too small proportion of the file would be downloaded. This value would be more
useful in case the user wants to return as soon as possible and do some other
processing before playing the media.

Whatever the size of the initial chunk, the torrent will continue downloading in
the background after the `seek()` function returns.  

A progress dialog box will be displayed while the `seek()` function runs:
it will show the amount of the initial chunk that has been downloaded. The user
can abort this initial download at anytime: it will exit the function without
any error and the download will continue in the background.  
Note that any new call to `seek()` will resume downloading from the existing
file.

## Examples

With this snippet you may download a media and play it whenever the initial
chunk was downloaded:

```python
import os.path
import json
from urllib.parse import quote_plus
import sys

import xbmcgui
import xbmcplugin

# Create an URL-encoded path of the torrent to download with the protocol as prefix
vfs_url = "torrent://{}".format(quote_plus(torrent_url))

# Call the add-on via xbmcvfs library
torrent = xbmcvfs.File(vfs_url)

# Start downloading the file
if torrent.seek(1, 0):

  # Get information about the torrent
  torrent_info = json.loads(torrent.read())

  # Build the path to the first file in the torrent
  file_path = os.path.join(torrent_info["save_path"],
                           torrent_info["files"][0]["path"])

  # Play the file
  xbmcplugin.setResolvedUrl(handle=int(sys.argv[1]),
                            succeeded=True,
                            listitem=xbmcgui.ListItem(path=file_path))
else:
  xbmcgui.Dialog().notification(heading="Error",
                                message="Could not download the file")
```

## Download folder

All the files will be downloaded in the folder `special://temp/vfs.libtorrent`.  
See [here](https://kodi.wiki/view/Special_protocol) for more information about
Kodi's special protocol.

A setting allows you to remove this folder completely when Kodi exits (off by
default).

## Notes about the format of URL

The format of Kodi's URL including protocol is:

`<PROTOCOL>://<USERNAME>:<PASSWORD>@<HOSTNAME>:<PORT>/<FILENAME>?<OPTIONS>`

In the context of this add-on:
* `<PROTOCOL>` is `torrent`
* the credentials (`<USERNAME>:<PASSWORD>@`) and the port (`:<PORT>`) are omitted

Because paths use slashes as separators on some systems, the part of the path
before the first slash will be interpreted as the hostname (`<HOSTNAME>`)
whereas the remaining part of the path will be interpreted as the filename
(`<FILENAME>`) which is not convenient for getting the full path.  
To solve this, the path must be URL encoded (for instance using python urllib's
`quote_plus` method): the add-on will automatically decode the hostname so the
full path will be available from `url.hostname` in the C++ APIs.
